NPROC ?= $(shell nproc --all)

ROFI ?= /usr/bin/rofi
SXHKD ?= /usr/local/bin/sxhkd
BSPWM ?= /usr/local/bin/bspwm
POLYBAR ?= /usr/local/bin/polybar
LIGHT ?= /usr/local/bin/light

.PHONY: dependencies

all: rofi bspwm

rofi: $(ROFI)
$(ROFI):
	sudo apt install -y rofi

bspwm: $(BSPWM)
.ONESHELL:
$(BSPWM): dependencies $(SXHKD)
	sudo apt install -y i3lock
	$(eval TMPDIR := $(shell mktemp --directory))
	git clone --depth 1 -b 0.9.5 https://github.com/baskerville/bspwm.git ${TMPDIR}
	cd ${TMPDIR}
	make -j${NPROC}
	sudo make install 
	sudo cp contrib/freedesktop/bspwm.desktop /usr/share/xsessions/
	cd -
	rm -rf ${TMPDIR}

sxhkd: $(SXHKD)
.ONESHELL:
$(SXHKD): dependencies
	$(eval TMPDIR := $(shell mktemp --directory))
	git clone --depth 1 -b 0.5.9 https://github.com/baskerville/sxhkd.git ${TMPDIR}
	cd ${TMPDIR}
	make -j${NPROC}
	sudo make install 
	cd -
	rm -rf ${TMPDIR}

polybar: $(POLYBAR)
.ONESHELL:
$(POLYBAR): dependencies
	$(eval TMPDIR := $(shell mktemp --directory))
	git clone --depth 1 --recursive https://github.com/jaagr/polybar.git ${TMPDIR}
	mkdir -p ${TMPDIR}/build
	cd ${TMPDIR}/build
	cmake ..
	make -j${NPROC}
	sudo make install

dependencies:
	# Install bspwm, polybar dependencies 
	# This works in Ubuntu 18.04 (and probably 18.10). Not sure about 16.04/16.10.
	sudo apt-get install -y \
		cmake \
		cmake-data \
		libcairo2-dev \
		libnl-genl-3-dev \
		libpulse-dev \
		libxcb-composite0-dev \
		libxcb-cursor-dev \
		libxcb-ewmh-dev \
		libxcb-icccm4-dev \
		libxcb-image0-dev \
		libxcb-keysyms1-dev \
		libxcb-randr0-dev \
		libxcb-shape0-dev \
		libxcb-util0-dev \
		libxcb-xinerama0-dev \
		libxcb-xrm-dev \
		libxcb1-dev \
		pkg-config \
		python-xcbgen \
		xcb-proto

light: $(LIGHT)
$(LIGHT):
	$(eval TMPDIR := $(shell mktemp --directory))
	wget -P ${TMPDIR} https://github.com/haikarainen/light/releases/download/v1.2/light_1.2_amd64.deb
	sudo dpkg -i ${TMPDIR}/light_1.2_amd64.deb
	rm -rf ${TMPDIR}
