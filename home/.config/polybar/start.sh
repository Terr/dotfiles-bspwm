#!/bin/sh

WORKING_CWD="$( cd "$(dirname "$0")" ; pwd -P )"

if [ -z $IF_WLAN ]; then
    export IF_WLAN=$(ip link show up|grep --max-count=1 wlp|cut -d':' -f 2|xargs)  # xargs trims the output
fi
if [ -z $IF_ETH ]; then
    export IF_ETH=$(ip link show up|grep --max-count=1 enp|cut -d':' -f 2|xargs)  # xargs trims the output
fi

polybar --config=${WORKING_CWD}/config --reload default
